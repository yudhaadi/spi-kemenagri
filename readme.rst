###################
This Web Application Use Code Igniter 3
###################


Server Requirement : 

 - PHP 7.0
 - CodeIgniter 3

*******************
Information
*******************

- Bootstrap
- JavaScript

**************************
Changelog and New Features
**************************

You can find a list of all changes for each release in the `user
guide change log <https://github.com/bcit-ci/CodeIgniter/blob/develop/user_guide_src/source/changelog.rst>`_.

*******************
Server Requirements
*******************

PHP version 5.6 or newer is recommended.

It should work on 5.3.7 as well, but we strongly advise you NOT to run
such old versions of PHP, because of potential security and performance
issues, as well as missing features.

************
Installation
************

- Masukkan ke dalam Htdocs xampp
- Import Database di folder database ke phpmyadmin
- Setting config -> database -> sesuaikan dengan settingan xampp kalian 

*******
License
*******

Halal buat di download asal ijin doloooo yaa 

Please see the `license
agreement <https://github.com/bcit-ci/CodeIgniter/blob/develop/user_guide_src/source/license.rst>`_.

fitur yang terdapat dalam web app ini adalah CRUD standar , role tiap user dll.


Pure Code By : Yudha Prakoso
Admin Template By : Stisla
