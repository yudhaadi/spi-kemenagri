<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en-US">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Sistem Informasi Akademik</title>
  <meta name="description"
    content="Creative CV is a HTML resume template for professionals. Built with Bootstrap 4, Now UI Kit and FontAwesome, this modern and responsive design template is perfect to showcase your portfolio, skils and experience." />
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet">
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
  <link href="<?php echo base_url('assets2') ?>/css/aos.css" rel="stylesheet">
  <link href="<?php echo base_url('assets2') ?>/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?php echo base_url('assets2') ?>/styles/main.css" rel="stylesheet">
</head>

<body id="top">
  <header>
    <div class="profile-page sidebar-collapse">
      <nav class="navbar navbar-expand-lg fixed-top navbar-transparent bg-primary" color-on-scroll="400">
        <div class="container">
          <div class="navbar-translate"><a class="navbar-brand" href="#" rel="tooltip">SIAU</a>
            <button class="navbar-toggler navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation"
              aria-controls="navigation" aria-expanded="false" aria-label="Toggle navigation"><span
                class="navbar-toggler-bar bar1"></span><span class="navbar-toggler-bar bar2"></span><span
                class="navbar-toggler-bar bar3"></span></button>
          </div>
          <div class="collapse navbar-collapse justify-content-end" id="navigation">
            <ul class="navbar-nav">
              <li class="nav-item"><a class="nav-link smooth-scroll" href="#about">About</a></li>

              <li class="nav-item"><a class="nav-link smooth-scroll" href="#portfolio">Facility</a></li>
              <li class="nav-item"><a class="nav-link smooth-scroll" href="#experience">News</a></li>
              <li class="nav-item"><a class="nav-link smooth-scroll" href="#contact">Contact</a></li>
              <li class="nav-item"><a class="nav-link smooth-scroll" href="<?php echo base_url('LoginController/index')?>" target="_blank">Login</a></li>
            </ul>
          </div>
        </div>
      </nav>
    </div>
  </header>
  <div class="page-content">
    <div>
      <div class="profile-page">
        <div class="wrapper">
          <div class="page-header page-header-small" filter-color="green">
            <div class="page-header-image" data-parallax="true" style="background-image: url('<?php echo base_url('assets2') ?>/images/wisudsky.jpg');">
            </div>
            <div class="container">
              <div class="content-center">
                <div class="cc-profile-image"><a href="#"><img src="<?php echo base_url('assets2') ?>/images/uhamkalogo.png" alt="Image" /></a></div>
                <div class="h2 title">Universitas Prof.Dr.Hamka Muhamamdiyah</div>
                <p class="category text-white">Fastabiqul Khoirot</p>
              </div>
            </div>
            <div class="section">
              <div class="container">
                <div class="button-container"><a class="btn btn-default btn-round btn-lg btn-icon"
                    href="https://www.facebook.com/uhamkaofficial/" rel="tooltip" title="Follow me on Facebook"><i
                      class="fa fa-facebook"></i></a><a class="btn btn-default btn-round btn-lg btn-icon"
                    href="https://twitter.com/uhamkaid" rel="tooltip" title="Follow me on Twitter"><i
                      class="fa fa-twitter"></i></a><a class="btn btn-default btn-round btn-lg btn-icon"
                    href="https://www.instagram.com/ft_uhamka/" rel="tooltip" title="Follow me on Instagram"><i
                      class="fa fa-instagram"></i></a></div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="section" id="about">
        <div class="container">
          <div class="card" data-aos="flip-up" data-aos-offset="10">
            <div class="row">
              <div class="col-lg-6 col-md-12">
                <div class="card-body">
                  <div class="h4 mt-0 title">About Uhamka</div>
                  <p> Universitas Muhammadiyah Prof. DR. HAMKA (selanjutnya disebut UHAMKA) merupakan salah satu
                    perguruan tinggi swasta milik Persyarikatan Muhammadiyah yang berlokasi di Jakarta Sebagai salah
                    satu amal usaha Muhammadiyah </p>

                  <p> UHAMKA adalah perguruan
                    tinggi berakidah Islam yang bersumber pada Al Quran dan As-Sunah serta berasaskan
                    Pancasila dan UUD 1945 yang melaksanakan tugas caturdharma Perguruan Tinggi Muhammadiyah,
                    yaitu menyelenggarakan pembinaan ketakwaan dan keimanan kepada Allah SWT., pendidikan dan
                    pengajaran, penelitian,
                    dan pengabdian pada masyarakat menurut tuntunan Islam.
                    <a href="https://uhamka.ac.id/history/" target="_blank">Learn More</a></p>
                </div>
              </div>
              <div class="col-lg-6 col-md-12">
                <div class="card-body">
                  <div class="h4 mt-0 title">Information</div>

                  <div class="row mt-3">
                    <div class="col-sm-4"><strong class="text-uppercase">Email:</strong></div>
                    <div class="col-sm-8">info@uhamka.ac.id</div>
                  </div>
                  <div class="row mt-3">
                    <div class="col-sm-4"><strong class="text-uppercase">Phone:</strong></div>
                    <div class="col-sm-8">+62(021) 7394451</div>
                  </div>
                  <div class="row mt-3">
                    <div class="col-sm-4"><strong class="text-uppercase">Address:</strong></div>
                    <div class="col-sm-8">Jl. Limau 2 Kebayoran Baru, Jakarta Selatan</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="section" id="about">
        <div class="container">
          <div class="card" data-aos="flip-up" data-aos-offset="10">
            <div class="row">
              <div class="col-lg-6 col-md-12">
                <div class="card-body">
                  <div class="h4 mt-0 title text-justify">Visi dan Misi </div>
                  <p> Universitas utama yang menghasilkan lulusan unggul dalam kecerdasan spiritual, intelektual,
                    emosional, dan sosial. </p>

                  <div class="h4 mt-0 title">Tujuan</div>
                  <div class="text-justify">
                    <p>Menyiapkan intelektual yang beriman dan bertakwa pada Allah SWT. berakhlak mulia</p>
                    <p>Mengembangkan dan menyebarluaskan berbagai ilmu pengetahuan Panduan UHAMKA serta pemanfaatannya
                      untuk memajukan Islam</p>
                    <p>UHAMKA sebagai pusat unggulan gerakan dakwah Muhammadiyah.</p>
                    <p>Menyiapkan Kader persyarikatan, kader umat, dan kader bangsa dalam rangka mewujudkan cita-cita
                      Muhammadiyah sebagai penggerak dakwah amar ma’ruf nahi munkar</p>
                    <a href="https://uhamka.ac.id/vision-mission-goals/" target="_blank">Learn More</a></p>
                  </div>
                </div>
              </div>
              <div class="col-lg-6 col-md-12">
                <div class="card-body">
                  <div class="h4 mt-0 title">Keunggulan Lulusan Uhamka</div>
                  <div class="h5 mt-0 title">1.Cerdas Spiritual</div>
                  <p>Taat Mengamalkan Ajaran Agama, Rajin Beribadah, Berakhlak Mulia, Berhati Nurani, Dan Layak Menjadi
                    Teladan</p>
                  <div class="h5 mt-0 title">2.Cerdas intelektual</div>
                  <p>Smart, Kreatif, Inovatif, Obyektif, Tangkas, Menjadi solusi bagi masyarakat</p>
                  <div class="h5 mt-0 title">3.Cerdas emosional</div>
                  <p>Sadar akan diri sendiri, Berprinsip lebih baik memberi dari pada menerima, Berempati / tanpa
                    selira, Bersemangat untuk berprestasi</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>



      <div class="section" id="portfolio">
        <div class="container">
          <div class="row">
            <div class="col-md-6 ml-auto mr-auto">
              <div class="h4 text-center mb-4 title">FACILITY</div>
              <div class="nav-align-center">
                <ul class="nav nav-pills nav-pills-primary" role="tablist">
                  <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#web-development"
                      role="tablist"><i class="fa fa-laptop" aria-hidden="true"></i></a></li>

                </ul>
              </div>
            </div>
          </div>
          <div class="tab-content gallery mt-5">
            <div class="tab-pane active" id="web-development">
              <div class="ml-auto mr-auto">
                <div class="row">
                  <div class="col-md-6">
                    <div class="cc-porfolio-image img-raised" data-aos="flip-up" data-aos-anchor-placement="top-bottom">
                      <a href="#web-development">
                        <figure class="cc-effect"><img src="<?php echo base_url('assets2') ?>/images/asrama.png" alt="Image" />
                          <figcaption>
                            <div class="h4">Home Stay</div>
                            <p>UHAMKA</p>
                          </figcaption>
                        </figure>
                      </a></div>
                    <div class="cc-porfolio-image img-raised" data-aos="flip-up" data-aos-anchor-placement="top-bottom">
                      <a href="#web-development">
                        <figure class="cc-effect"><img src="<?php echo base_url('assets2') ?>/images/lab.jpeg" alt="Image" />
                          <figcaption>
                            <div class="h4">Laboratorium Science</div>
                            <p>UHAMKA</p>
                          </figcaption>
                        </figure>
                      </a></div>
                  </div>
                  <div class="col-md-6">
                    <div class="cc-porfolio-image img-raised" data-aos="flip-up" data-aos-anchor-placement="top-bottom">
                      <a href="#web-development">
                        <figure class="cc-effect"><img src="<?php echo base_url('assets2') ?>/images/kelas.jpg" alt="Image" />
                          <figcaption>
                            <div class="h4">Class</div>
                            <p>UHAMKA</p>
                          </figcaption>
                        </figure>
                      </a></div>
                    <div class="cc-porfolio-image img-raised" data-aos="flip-up" data-aos-anchor-placement="top-bottom">
                      <a href="#web-development">
                        <figure class="cc-effect"><img src="<?php echo base_url('assets2') ?>/images/labkom.jpg" alt="Image" />
                          <figcaption>
                            <div class="h4">Computer Lab</div>
                            <p>UHAMKA</p>
                          </figcaption>
                        </figure>
                      </a></div>

                    <div class="cc-porfolio-image img-raised" data-aos="flip-up" data-aos-anchor-placement="top-bottom">
                      <a href="#web-development">
                        <figure class="cc-effect"><img src="<?php echo base_url('assets2') ?>/images/canteen.jpg" alt="Image" />
                          <figcaption>
                            <div class="h4">Canteen</div>
                            <p>UHAMKA</p>
                          </figcaption>
                        </figure>
                      </a></div>

                    <div class="cc-porfolio-image img-raised" data-aos="flip-up" data-aos-anchor-placement="top-bottom">
                      <a href="#web-development">
                        <figure class="cc-effect"><img src="<?php echo base_url('assets2') ?>/images/lapanganfutsal.jpg" alt="Image" />
                          <figcaption>
                            <div class="h4">Futsal Field</div>
                            <p>UHAMKA</p>
                          </figcaption>
                        </figure>
                      </a></div>


                  </div>
                </div>
              </div>
            </div>

            <div class="section" id="experience">
              <div class="container cc-experience">
                <div class="h4 text-center mb-4 title">NEWS</div>
                <div class="card">
                  <div class="row">
                    <div class="col-md-3 bg-primary" data-aos="fade-right" data-aos-offset="50" data-aos-duration="500">
                      <div class="card-body cc-experience-header">
                        <p>March 2019 - Present</p>
                        <div class="h5">Internship</div>
                      </div>
                    </div>
                    <div class="col-md-9" data-aos="fade-left" data-aos-offset="50" data-aos-duration="500">
                      <div class="card-body text-justify">
                        <div class="h5">Program Magang KBRI di Malaysia</div>
                        <p>Mahasiswa Fakultas Ilmu Sosial dan Ilmu Politik (FISIP) dan Fakultas Keguruan dan Ilmu
                          Pendidikan (FKIP) Universitas Muhammadiyah Prof. Dr. HAMKA (UHAMKA) mengikuti program magang
                          di Kedutaan Besar Republik Indonesia di Kuala Lumpur, Malaysia.

                          Mahasiswa peserta magang tersebut berasal dari Program Studi Ilmu Komunikasi (2 orang) yakni
                          Malik Abdul Aziz dan Mega Anisa Suandi dan Program Studi Pendidikan Ekonomi (3 orang) yakni
                          Nikmawanti, Hurinnabila Arfah, dan Wardah Safitri.</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="card">
                  <div class="row">
                    <div class="col-md-3 bg-primary" data-aos="fade-right" data-aos-offset="50" data-aos-duration="500">
                      <div class="card-body cc-experience-header">
                        <p>March 2019</p>
                        <div class="h5">Conferences</div>
                      </div>
                    </div>
                    <div class="col-md-9" data-aos="fade-left" data-aos-offset="50" data-aos-duration="500">
                      <div class="card-body text-justify">
                        <div class="h5">Mahasiswa FISIP UHAMKA Hadiri Program VLDP & ASR ASEAN INTERNATIONAL</div>
                        <p>Fendriano Mafrandana (Pepen) mahasiswa FISIP UHAMKA 2016 Program Studi Ilmu Komunikasi
                          (Public relation) kembali mengharumkan nama Indonesia di kancah international.

                          Pepen menghadiri program VLDP & ASR ASEAN INTERNATIONAL di Hotel Marriott, Putra Jaya,
                          Malaysia. Penyelenggara ini adalah YSS ASEAN (yayasan sukarelawan siswa) dibawah naungan
                          Kementerian Pendidikan Malaysia. Acara ini berlangsung pada tanggal 3-7 Desember 2019, negara
                          yang ikut dalam program ini adalah negara se-ASEAN dan China Hongkong.</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="card">
                  <div class="row">
                    <div class="col-md-3 bg-primary" data-aos="fade-right" data-aos-offset="50" data-aos-duration="500">
                      <div class="card-body cc-experience-header">
                        <p>December 2019</p>
                        <div class="h5">UHAMKA CHOIR</div>
                      </div>
                    </div>
                    <div class="col-md-9" data-aos="fade-left" data-aos-offset="50" data-aos-duration="500">
                      <div class="card-body">
                        <div class="h5">Intern</div>
                        <p>Universitas Muhammadiyah Prof. Dr. Hamka (UHAMKA) mengirim 5 mahasiswa untuk menjadi delegasi
                          di acara Indonesia Islamic youth Leaders Summit 2019 dengan tema “Strengthening The role Of
                          Muslim Youth To Consolidate The Ummah” yang diadakan di Gedung DPR/MPR RI 22-26 November
                          2019.Nur Syaidah Mahasiswi Fakultas Ilmu Sosial dan Ilmu Politik (FISIP) salah satu delegasi
                          dari UHAMKA mewakili Qatar berhasil mendapat penghargaan The Most Outstanding Diplomat. “Saya
                          merasa sangat senang dan bangga bisa menjadi sala satu delegasi dalam acara internasional yang
                          diikuti lebih dari 20 negara lainnya ditambah bisa mendapat penghargaan sebagai ‘The Most
                          Outstanding Diplomat’ karena dari Indonesia sendiri hanya 3 orang yang berhasil mendapat
                          penghargaan dalam acara ini. ” ujar Syaidah.</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="section">
              <div class="container cc-education">
                <div class="h4 text-center mb-4 title">Education</div>
                <div class="card">
                  <div class="row">
                    <div class="col-md-3 bg-primary" data-aos="fade-right" data-aos-offset="50" data-aos-duration="500">
                      <div class="card-body cc-education-header">
                        <p>1337 Hijriah - Now</p>
                        <div class="h5">FKIP</div>
                      </div>
                    </div>
                    <div class="col-md-9" data-aos="fade-left" data-aos-offset="50" data-aos-duration="500">
                      <div class="card-body">
                        <div class="h5">Fakultas Keguruan & Ilmu Pendidikan</div>
                        <p class="category">Universitas Prof.Dr.Hamka Muhammadiyah</p>
                        <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Fugiat voluptas dolorum ad eius et
                          similique quod officia, consectetur sunt. Iste recusandae ea optio rem tenetur, unde ipsum
                          omnis nihil. Voluptates!</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="card">
                  <div class="row">
                    <div class="col-md-3 bg-primary" data-aos="fade-right" data-aos-offset="50" data-aos-duration="500">
                      <div class="card-body cc-education-header">
                        <p>1337 Hijriah - Now</p>
                        <div class="h5">FT</div>
                      </div>
                    </div>
                    <div class="col-md-9" data-aos="fade-left" data-aos-offset="50" data-aos-duration="500">
                      <div class="card-body">
                        <div class="h5">Fakultas Teknik</div>
                        <p class="category">Universitas Prof.Dr.Hamka Muhammadiyah</p>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Unde pariatur minima consequatur,
                          corporis numquam voluptate qui soluta provident voluptas, architecto aut veniam dolor ad
                          aliquid dolores laudantium voluptatibus fugit exercitationem.</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="card">
                  <div class="row">
                    <div class="col-md-3 bg-primary" data-aos="fade-right" data-aos-offset="50" data-aos-duration="500">
                      <div class="card-body cc-education-header">
                        <p>1337 Hijriah - Now</p>
                        <div class="h5">FEB</div>
                      </div>
                    </div>
                    <div class="col-md-9" data-aos="fade-left" data-aos-offset="50" data-aos-duration="500">
                      <div class="card-body">
                        <div class="h5">Fakultas Ekonomi & Bisnis</div>
                        <p class="category">Universitas Prof.Dr.Hamka Muhammadiyah</p>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Distinctio itaque neque temporibus
                          corporis. Amet totam at officia dolorum tempora, eum, animi porro consequuntur voluptas
                          distinctio odit aspernatur inventore qui error.</p>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="card">
                  <div class="row">
                    <div class="col-md-3 bg-primary" data-aos="fade-right" data-aos-offset="50" data-aos-duration="500">
                      <div class="card-body cc-education-header">
                        <p>1337 Hijriah - Now</p>
                        <div class="h5">FFS</div>
                      </div>
                    </div>
                    <div class="col-md-9" data-aos="fade-left" data-aos-offset="50" data-aos-duration="500">
                      <div class="card-body">
                        <div class="h5">Fakultas Farmasi & Sains</div>
                        <p class="category">Universitas Prof.Dr.Hamka Muhammadiyah</p>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Unde pariatur minima consequatur,
                          corporis numquam voluptate qui soluta provident voluptas, architecto aut veniam dolor ad
                          aliquid dolores laudantium voluptatibus fugit exercitationem.</p>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="card">
                  <div class="row">
                    <div class="col-md-3 bg-primary" data-aos="fade-right" data-aos-offset="50" data-aos-duration="500">
                      <div class="card-body cc-education-header">
                        <p>1337 Hijriah - Now</p>
                        <div class="h5">FIKES</div>
                      </div>
                    </div>
                    <div class="col-md-9" data-aos="fade-left" data-aos-offset="50" data-aos-duration="500">
                      <div class="card-body">
                        <div class="h5">Fakultas Ilmu Kesehatan</div>
                        <p class="category">Universitas Prof.Dr.Hamka Muhammadiyah</p>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Unde pariatur minima consequatur,
                          corporis numquam voluptate qui soluta provident voluptas, architecto aut veniam dolor ad
                          aliquid dolores laudantium voluptatibus fugit exercitationem.</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="card">
                  <div class="row">
                    <div class="col-md-3 bg-primary" data-aos="fade-right" data-aos-offset="50" data-aos-duration="500">
                      <div class="card-body cc-education-header">
                        <p>1337 Hijriah - Now</p>
                        <div class="h5">FISIP</div>
                      </div>
                    </div>
                    <div class="col-md-9" data-aos="fade-left" data-aos-offset="50" data-aos-duration="500">
                      <div class="card-body">
                        <div class="h5">Fakultas Ilmu Sosial & Politik</div>
                        <p class="category">Universitas Prof.Dr.Hamka Muhammadiyah</p>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Unde pariatur minima consequatur,
                          corporis numquam voluptate qui soluta provident voluptas, architecto aut veniam dolor ad
                          aliquid dolores laudantium voluptatibus fugit exercitationem.</p>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="card">
                  <div class="row">
                    <div class="col-md-3 bg-primary" data-aos="fade-right" data-aos-offset="50" data-aos-duration="500">
                      <div class="card-body cc-education-header">
                        <p>1337 Hijriah - Now</p>
                        <div class="h5">FK</div>
                      </div>
                    </div>
                    <div class="col-md-9" data-aos="fade-left" data-aos-offset="50" data-aos-duration="500">
                      <div class="card-body">
                        <div class="h5">Fakultas Kedokteran</div>
                        <p class="category">Universitas Prof.Dr.Hamka Muhammadiyah</p>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Unde pariatur minima consequatur,
                          corporis numquam voluptate qui soluta provident voluptas, architecto aut veniam dolor ad
                          aliquid dolores laudantium voluptatibus fugit exercitationem.</p>
                      </div>
                    </div>
                  </div>
                </div>



              </div>
            </div>
            <div class="section" id="reference">
              <div class="container cc-reference">
                <div class="h4 mb-4 text-center title">References</div>
                <div class="card" data-aos="zoom-in">
                  <div class="carousel slide" id="cc-Indicators" data-ride="carousel">
                    <ol class="carousel-indicators">
                      <li class="active" data-target="#cc-Indicators" data-slide-to="0"></li>
                      <li data-target="#cc-Indicators" data-slide-to="1"></li>
                      <li data-target="#cc-Indicators" data-slide-to="2"></li>
                    </ol>
                    <div class="carousel-inner">
                      <div class="carousel-item active">
                        <div class="row">
                          <div class="col-lg-2 col-md-3 cc-reference-header"><img src="<?php echo base_url('assets2') ?>/images/saepul.jpg" alt="Image" />
                            <div class="h5 pt-2">Dr. H. Saefullah, <M class="Pd"></M>
                            </div>
                            <p class="category">Secretarist Government Jakarta City</p>
                          </div>
                          <div class="col-lg-10 col-md-9">
                            <p> "Saya sebagai Alumni dari UHAMKA turut bangga atas banyak prestasi yang didapat selama
                              ini, UHAMKA selalu terdepan didalam bidang apapun dan tidak ketinggalan untuk mendidik
                              sumber daya manusia nya untuk menghadapi evolusi industri yang semakin banyak menggunakan
                              teknologi"</p>
                          </div>
                        </div>
                      </div>
                      <div class="carousel-item">
                        <div class="row">
                          <div class="col-lg-2 col-md-3 cc-reference-header"><img src="<?php echo base_url('assets2') ?>/images/nasir.jpg" alt="Image" />
                            <div class="h5 pt-2">Dr.H. Haedar Nasir, M.Si.</div>
                            <p class="category">Lead Muhammadiyah Central</p>
                          </div>
                          <div class="col-lg-10 col-md-9 text-justify">
                            <p> "UHAMKA tidak hanya berkembang di tempat itu saja,saya turut bangga dengan UHAMKA karena
                              bisa bersaing dengan kampus-kampus Negri
                              segelincir prestasi nya di jenjang Internasional" </p>
                          </div>
                        </div>
                      </div>
                      <div class="carousel-item">
                        <div class="row">
                          <div class="col-lg-2 col-md-3 cc-reference-header"><img src="<?php echo base_url('assets2') ?>/images/jalal.jpg" alt="Image" />
                            <div class="h5 pt-2">Prof. Dr. Fasli Jalal, Ph.D.</div>
                            <p class="category">Minister Of Edu</p>
                          </div>
                          <div class="col-lg-10 col-md-9">
                            <p> "Saya sangat percaya dengan pengalaman UHAMKA mengelola pendidikan tinggi dan dapat
                              menyiapkan SDM Berkualitas untuk menyongsong negara yang sedang banyak membutuhkan
                              generasi muda untuk meneruskan berjuangan bangsa ini" </p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="section" id="contact">
              <div class="cc-contact-information" style="background-image: url('<?php echo base_url('assets2') ?>/images/map.jpg');">
                <div class="container">
                  <div class="cc-contact">
                    <div class="row">
                      <div class="col-md-9">
                        <div class="card mb-0" data-aos="zoom-in">
                          <div class="h4 text-center title">Contact Me</div>
                          <div class="row">
                            <div class="col-md-6">
                              <div class="card-body">
                                <form action="https://uhamka.ac.id/" method="POST">
                                  <div class="p pb-3"><strong>Contact if you want register to UHAMKA </strong></div>
                                  <div class="row mb-3">
                                    <div class="col">
                                      <div class="input-group"><span class="input-group-addon"><i
                                            class="fa fa-user-circle"></i></span>
                                        <input class="form-control" type="text" name="name" placeholder="Name"
                                          required="required" />
                                      </div>
                                    </div>
                                  </div>

                                  <div class="row mb-3">
                                    <div class="col">
                                      <div class="input-group"><span class="input-group-addon"><i
                                            class="fa fa-envelope"></i></span>
                                        <input class="form-control" type="email" name="_replyto" placeholder="E-mail"
                                          required="required" />
                                      </div>
                                    </div>
                                  </div>
                                  <div class="row mb-3">
                                    <div class="col">
                                      <div class="form-group">
                                        <textarea class="form-control" name="message" placeholder="Your Message"
                                          required="required"></textarea>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col">
                                      <button class="btn btn-primary" type="submit">Send</button>
                                    </div>
                                  </div>
                                </form>
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="card-body">
                                <p class="mb-0"><strong>Address </strong></p>
                                <p class="pb-2">Jl. Limau 2 Kebayoran Baru, Jakarta Selatan </p>
                                <p class="mb-0"><strong>Phone</strong></p>
                                <p class="pb-2">+62(021) 7394451</p>
                                <p class="mb-0"><strong>Email</strong></p>
                                <p>info@uhamka.ac.id</p>
                                <a href="#" class="btn btn-primary active" type="submit">Location</a>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <footer class="footer">
          <div class="container text-center"><a class="cc-facebook btn btn-link"
              href="https://www.facebook.com/uhamkaofficial/"><i class="fa fa-facebook fa-2x "
                aria-hidden="true"></i></a><a class="cc-twitter btn btn-link " href="#"><i class="fa fa-twitter fa-2x "
                aria-hidden="true"></i></a><a class="cc-google-plus btn btn-link" href="#"><i
                class="fa fa-google-plus fa-2x" aria-hidden="true"></i></a><a class="cc-instagram btn btn-link"
              href="#"><i class="fa fa-instagram fa-2x " aria-hidden="true"></i></a>
          </div>
          <div class="h4 title text-center">KELOMPOK IV</div>
          <div class="text-center text-muted">
            <p>&copy; UHAMKA.AC.ID All rights reserved(copyright Strike).<br>Design by Front-end - <a class="credit"
                href="#" target="_blank">Front-end</a></p>
          </div>
        </footer>
        <script src="<?php echo base_url('assets2') ?>/js/core/jquery.3.2.1.min.js"></script>
        <script src="<?php echo base_url('assets2') ?>/js/core/popper.min.js"></script>
        <script src="<?php echo base_url('assets2') ?>/js/core/bootstrap.min.js"></script>
        <script src="<?php echo base_url('assets2') ?>/js/now-ui-kit.js?v=1.1.0"></script>
        <script src="<?php echo base_url('assets2') ?>/js/aos.js"></script>
        <script src="<?php echo base_url('assets2') ?>/scripts/main.js"></script>
</body>

</html>