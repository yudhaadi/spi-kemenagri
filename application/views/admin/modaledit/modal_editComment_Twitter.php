<?php
$this->load->view('templates/dashboard_header');
?>

<?php
$this->load->view('admin/templates/admin_sidebar');
?>



<div class="modal fade" tabindex="-1" role="dialog" id="modal_edit_comment_twitter">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" >Pertanyaan & Tanggapan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            
            

            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="profil-tab" data-toggle="tab" href="#profil" role="tab" aria-controls="profil" aria-selected="true">Profil</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Edit Data</a>
                </li>
                <!-- <li class="nav-item">
                    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Contact</a>
                </li> -->
            </ul>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="profil" role="tabpanel" aria-labelledby="profil-tab">

                    <div class="card">
                        <div class="card-body">
                            <!-- <div class="section-title mt-0" style="text-align: center">Pertanyaan & Tanggapan</div> -->
                            
                            <div class="card">
                                <div class="card-header">
                                    <h4>Pertanyaan Responden</h4>
                                </div>
                                <div class="card-body bg-whitesmoke" readonly="" id="pertanyaan_info" > 
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header">
                                    <h4>Tanggapan Admin</h4>
                                </div>
                                <div class="card-body bg-whitesmoke" readonly="" id="tanggapan_info" > 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">

                    <form method="POST" action="<?php echo base_url('Admin/updateTwitter') ?>">

                        <input type="hidden" name="id_data_twitter" id="id_data_twitter">

                        <div class="modal-body" >

                            <div class="form-group" >
                                <label>Pertanyaan</label>
                                <textarea class="form-control " name="pertanyaan" id="pertanyaan"></textarea>
                            </div>
                            
                            <div class="form-group">
                                <label>Tanggapan</label>
                                <textarea class="form-control" name="tanggapan" id="tanggapan"></textarea>
                            </div>

                        </div>
                        <div class="modal-footer bg-whitesmoke br">

                            <button type="submit" class="btn btn-success">Simpan</button>
                            <button type="button" class="btn btn-warning" data-dismiss="modal">Batal</button>
                        </div>
                    </form>


                </div>
                <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">...</div>
            </div>







        </div>



    </div>
</div>






</div>


<!-- General JS Scripts -->
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
<script src="<?php echo base_url('assets') ?>/js/stisla.js"></script>

<!-- JS Libraies -->
<script src="<?php echo base_url('node_modules') ?>/prismjs/prism.js"></script>

<!-- Template JS File -->
<script src="<?php echo base_url('assets') ?>/js/scripts.js"></script>
<script src="<?php echo base_url('assets') ?>/js/custom.js"></script>

<!-- Page Specific JS File -->
<script src="<?php echo base_url('assets') ?>/js/page/bootstrap-modal.js"></script>