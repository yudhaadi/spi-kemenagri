<?php
$this->load->view('templates/dashboard_header');
?>

<?php
$this->load->view('templates/dashboard_navbar');
?>

<?php
$this->load->view('admin/templates/admin_sidebar');
?>

<style>
    .modal-backdrop {
        display: none;
    }
</style>

<?php

include 'modaledit/modal_edit-info.php';
?>


<?php

include 'modaltambah/modal_tambahMahasiswa.php';
?>


<body>
    <div id="app">
        <div class="main-wrapper">

            <!-- Main Content -->
            <div class="main-content">
                <section class="section">
                    <div class="section-header">
                        <h1>
                            Data Mahasiswa
                        </h1>
                        <h style="margin: 0 auto;" ></h><b id="datetime" style="color: green;"></b>
                    </div>

                    <!-- <div class="section-body">
                        <h2 class="section-title">DataTables</h2>
                        <p class="section-lead">
                            We use 'DataTables' made by @SpryMedia. You can check the full documentation <a href="https://datatables.net/">here</a>.
                        </p> -->

                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <!-- <h4>Data Mahasiswa</h4> -->
                                    <p class="text-muted font-13 m-b-30" style="position: absolute; right:16px">
                                        <button id="addBtn" style="width:133px;" class="btn btn-success icon-left btn-block" data-toggle="modal" data-target="#tambah_Modal_Mhs"> <i class="fas fa-plus-circle"></i> Tambah Data </button>
                                    </p>
                                </div>

                                <div class="card-body">
                                    <div class="table-responsive">
                                        <?php if (!empty($mahasiswa)) { ?>
                                            <table class="table table-striped" id="table-1">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center">No. </th>
                                                        <th>Nim</th>
                                                        <th>Nama Mahasiswa</th>
                                                        <th>Jenis Kelamin</th>
                                                        <th>QR Code</th>
                                                        <th>Aksi</th>

                                                        <!-- <th>Status</th>
                                                        <th>Action</th> -->
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php $no = 1;
                                                    foreach ($mahasiswa as $row) { ?>
                                                        <tr>
                                                            <td class="text-center" width="10%"><?php echo $no++ ?></td>
                                                            <td class="font-w600"><?php echo $row['nim'] ?></td>
                                                            <td class="font-w600"><?php echo $row['nama'] ?></td>
                                                            <td class="font-w600"><?php echo $row['jenis_kelamin'] ?></td>
                                                            <td><img style="width: 100px;" src="<?php echo base_url() . $row['qr_code']; ?>"></td>
                                                            <td>
                                                                <!-- <div style="margin: 10px 0px;"> -->
                                                                <a href="#" data-id="<?= $row['id_mhs'] ?>" data-nim="<?= $row['nim'] ?>" data-nama="<?= $row['nama'] ?>" data-email="<?= $row['email'] ?>" data-alamat="<?= $row['alamat'] ?>" data-no_telp="<?= $row['no_telp'] ?>" data-jenis_kelamin="<?= $row['jenis_kelamin'] ?>" data-tempat_lahir="<?= $row['tempat_lahir'] ?>" data-kewarganegaraan="<?= $row['kewarganegaraan'] ?>" data-agama="<?= $row['agama'] ?>" data-tgl_lahir="<?= $row['tgl_lahir'] ?>" style="border-radius: 30px" class="btn btn-icon icon-left btn-warning" data-toggle="modal" data-target="#modal_edit_mhs">
                                                                    <i class=" far fa-edit"></i>
                                                                    Detail
                                                                </a>
                                                                <!-- <a href="#" class="btn btn-icon icon-left btn-danger"><i class="fas fa-times"></i> Hapus</a></td> -->


                                                                <a href="<?= base_url('Admin/hapusMahasiswa/' . $row['id_mhs']) ?>" class="hapus">
                                                                    <button class="btn btn-icon icon-left btn-danger" type="button" data-toggle="tooltip" style="border-radius: 30px;">
                                                                        <i class=" fas fa-times"></i>
                                                                        Hapus
                                                                    </button>
                                                                </a>

                                                                <!-- </div> -->
                                                            </td>


                                                        </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } else { ?>
                    <!-- <div class="row"> -->
                    <!-- <div class="col-12"> -->
                    <!-- <div class="card"> -->
                    <!-- <div class="card-header">
                        <h4>Advanced Table</h4>
                    </div> -->
                    <!-- <div class="card-body"> -->

                    <!-- <div class="table-responsive"> -->
                    <!-- <table class="table table-striped" id="table-2">
                            <thead>
                                <tr>
                                    <th class="text-center">
                                        <div class="custom-checkbox custom-control">
                                            <input type="checkbox" data-checkboxes="mygroup" data-checkbox-role="dad" class="custom-control-input" id="checkbox-all">
                                            <label for="checkbox-all" class="custom-control-label">&nbsp;</label>
                                        </div>
                                    </th>
                                    <th class="text-center">No. </th>
                                    <th>Nim</th>
                                    <th>Nama Mahasiswa</th>
                                    <th>Email</th>
                                    <th>Alamat</th>
                                    <th>Jenis Kelamin</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table> -->
                <?php } ?>


                <!-- </div> -->
                <!-- </div> -->
                <!-- </div> -->
                <!-- </div> -->
                <!-- </div> -->
                <!-- </div> -->
                </section>
            </div>

        </div>
    </div>

   



    <!-- JS Libraies -->
    <script src="<?php echo base_url('node_modules') ?>/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url('node_modules') ?>/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>

    <script>
var dt = new Date();
document.getElementById("datetime").innerHTML = dt.toLocaleString();
</script>
    
    <script>
        $("#table-1").dataTable();



        // swal 
        $('.hapus').on("click", function(e) {
            e.preventDefault();
            var url = $(this).attr('href');
            Swal.fire({
                title: 'Anda Yakin?',
                text: "Data tidak bisa di kembalikan lagi!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    Swal.fire(
                        'Deleted!',
                        'Data Berhasil Dihapus.',
                        'success'
                    ).then(() => {
                        window.location.href = url
                    });
                }
            })
        });



        $('#modal_edit_mhs').on('show.bs.modal', function(e) {

            var div = $(e.relatedTarget);

            var id = div.data('id');
            var nim = div.data('nim');
            var nama = div.data('nama');
            var email = div.data('email');
            var alamat = div.data('alamat');
            var jenis_kelamin = div.data('jenis_kelamin');
            var no_telp = div.data('no_telp');
            var tempat_lahir = div.data('tempat_lahir');
            var kewarganegaraan = div.data('kewarganegaraan');
            var agama = div.data('agama');
            var tgl_lahir = div.data('tgl_lahir');



            var modal = $(this);
            modal.find('#id_mhs').attr("value", id);
            modal.find('#nim').attr("value", nim);
            modal.find('#nama').attr("value", nama);
            modal.find('#email').attr("value", email);
            modal.find('#alamat').attr("value", alamat);
            modal.find('#jenis_kelamin').attr("value", jenis_kelamin);
            modal.find('#no_telp').attr("value", no_telp);
            modal.find('#tempat_lahir').attr("value", tempat_lahir);
            modal.find('#kewarganegaraan').attr("value", kewarganegaraan);
            modal.find('#agama').attr("value", agama);
            modal.find('#tgl_lahir').attr("value", tgl_lahir);


            modal.find('#nim_info').text(nim);
            modal.find('#nama_info').text(nama);
            modal.find('#email_info').text(email);
            modal.find('#alamat_info').text(alamat);
            modal.find('#gender_info').text(jenis_kelamin);
            modal.find('#no_telp_info').text(no_telp);
            modal.find('#tempat_lahir_info').text(tempat_lahir);
            modal.find('#kewarganegaraan_info').text(kewarganegaraan);
            modal.find('#agama_info').text(agama);
            modal.find('#tgl_lahir_info').text(tgl_lahir);



        });
    </script>

    <?php

    $this->load->view('templates/dashboard_footer');

    ?>