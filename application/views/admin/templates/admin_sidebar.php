<div class="main-sidebar">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
            <a href="#">SISTEM PENDATAAN</a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
            <a href="index.html">St</a>
        </div>
        <ul class="sidebar-menu">

            <li class="menu-header">Dashboard</li>
            <li class="nav-item <?php echo (current_url() == base_url() . 'Admin/index') ? 'active' : '' ?>">
                <a href="<?php echo base_url('Admin/index') ?>" class=" nav-item <?php echo (current_url() == base_url() . 'Admin/index') ? 'active' : '' ?>"><i class="fas fa-home"></i><span>Dashboard</span></a>

            </li>


            <li class="menu-header">Management User</li>
            <li class="nav-item dropdown <?php echo (current_url() == base_url() . 'Admin/tampildataAdmin' || current_url() == base_url() . 'Admin/tampildataMahasiswa') ? 'active' : '' ?>">
                <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-user-edit"></i>
                    <span>Edit User</span></a>
                <ul class="dropdown-menu ">
                    <li class="<?php echo (current_url() == base_url() . 'Admin/tampildataAdmin') ? 'active' : '' ?>"><a href="<?php echo base_url('Admin/tampildataAdmin') ?>">Admin</a></li>
                </ul>
                <ul class="dropdown-menu ">
                    <li class="<?php echo (current_url() == base_url() . 'Admin/tampildataMahasiswa') ? 'active' : '' ?>"><a href="<?php echo base_url('Admin/tampildataMahasiswa') ?>">Mahasiswa Magang</a></li>
                </ul>
            </li>

            <li class="menu-header">Dumas Management</li>
            <li class="nav-item dropdown <?php echo (current_url() == base_url() . 'Admin/tampildataCommentIG') ? 'active' : '' ?>">
                <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"> <i class="fab fa-instagram"></i>
                    <span>Comment Instagram</span></a>
                <ul class="dropdown-menu">
                    <li class="<?php echo (current_url() == base_url() . 'Admin/tampildataCommentIG') ? 'active' : '' ?>">
                        <a class="nav-link" href="<?php echo base_url('Admin/tampildataCommentIG') ?>">Edit Data</a></li>

                </ul>
            </li>

            <li class="nav-item dropdown <?php echo (current_url() == base_url() . 'Admin/tampildataCommentTwitter') ? 'active' : ''  ?>">
                <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fab fa-twitter"></i>
                    <span>Comment Twitter</span></a>
                <ul class="dropdown-menu">
                    <li class="<?php echo (current_url() == 'Admin/tampildataCommentTwitter') ? "active" : "" ?>"><a href="<?php echo base_url('Admin/tampildataCommentTwitter') ?>">Edit Data</a></li>
                    <!-- <li><a class="nav-link" href="layout-transparent.html">Edit Nilai</a></li> -->
                </ul>

            </>

            <li class="nav-item dropdown <?php echo (current_url() == base_url() . 'Admin/tampildataCommentFB') ? 'active' : ''  ?>">
                <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fab fa-facebook"></i>
                    <span>Comment Facebook</span></a>
                <ul class="dropdown-menu">
                    <li class="<?php echo (current_url() == 'Admin/tampildataCommentFB') ? "active" : "" ?>"><a href="<?php echo base_url('Admin/tampildataCommentFB') ?>">Edit Data</a></li>
                    <!-- <li><a class="nav-link" href="layout-transparent.html">Edit Nilai</a></li> -->
                </ul>

            </li>

          

        </ul>

    </aside>
</div>