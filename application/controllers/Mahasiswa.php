<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mahasiswa extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('m_mhs');
    }

    public function index()
    {
        $data['title'] = 'Mahasiswa';
        $data['total_matkulmhs'] = $this->m_mhs->hitung_matkulmhs();
        $this->load->view('mahasiswa/dashboard_mahasiswa', $data);
    }

    public function tampildataMkmhs()
    {

        $data['title'] = 'Lihat MataKuliah';

        $data['lihat_matkul'] = $this->m_mhs->show_Mkmhs();

        $this->load->view('mahasiswa/mhs_matakuliah', $data);
    }
}
