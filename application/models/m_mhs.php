<?php



defined('BASEPATH') or exit('No direct script access allowed');

class m_mhs extends CI_Model
{


    var $tanda;

    public function show_Mkmhs()
    {
        $tanda = $this->session->userdata('nim');
        $this->db->where('nim', $tanda);
        $query = $this->db->query("SELECT * FROM nilai INNER JOIN jadwal ON nilai.kd_mk = jadwal.kd_mk WHERE nim = '$tanda'");
        return $query->result_array();
    }

    public function show_nilaimhs()
    {
        $tanda = $this->session->userdata('nim');
        $this->db->where('nim', $tanda);
        $query = $this->db->query("SELECT * FROM nilai WHERE nim = '$tanda'");
        return $query->result_array();
    }

    public function hitung_matkulmhs()
    {
        $skrg = $this->session->userdata('nim');
        $this->db->where('nim', $skrg);
        $query = $this->db->get('nilai');
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return 0;
        }
    }
}
